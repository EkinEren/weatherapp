<h1> Istanbul Local Weather App </h1>

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

<img src = "https://img.shields.io/badge/npm-5.6.0-green.svg"/> <img src = "https://img.shields.io/badge/node-v8.11.4-green.svg"/>

[Open Weather Map](https://openweathermap.org/) API is used to recieve dynamic weather data.

<h1>Usage</h1>

<ul>
<li>This application shows the current local weather in Istanbul.</li>
<li>This is a starter react project.</li>
<li>To get the weather for another city or according to your coordinates, check out the <a href = "https://openweathermap.org/current"> API documentation </a> of Open Weather Map. </li>
</ul>
