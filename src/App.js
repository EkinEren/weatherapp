import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const APIKEY = process.env.REACT_APP_WEATHER_API_KEY;
const url = `http://api.openweathermap.org/data/2.5/weather?q=Istanbul&id=524901&units=metric&APPID=${APIKEY}`
//const call = api.openweathermap.org/data/2.5/forecast?id=524901&APPID={APIKEY}

console.log(APIKEY);

class WeatherApp extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      post: [],
      city: '',
      country: '',
      weather: '',
      description: '',
      icon: '',
      curTime: ''
    }

  }

  componentDidMount() {
    this.fetchPost();
    setInterval(() => {
      this.setState({
        curTime: new Date().toLocaleString()
      })
    }, 1000)
  }


  componentDidUpdate(prevState) {
    if (JSON.stringify(this.state.post) !== JSON.stringify(prevState.post)) {
      this.fetchPost();
    }
  }

  componentWillUnmount() {

    clearInterval(this.curTime);

  }

  fetchPost = () => {

    fetch(url, { method: 'GET', cache: 'reload' })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState({
          post: data,
          city: data.name,
          country: data.sys.country,
          description: data.weather[0].description,
          weather: data.main.temp,
          icon: data.weather[0].icon

        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {

    const { city, country, icon, weather, description, curTime } = this.state

    return (
      <div>
        <h2>{city}, {country}</h2>
        <p>
          <img src={`http://openweathermap.org/img/w/${icon}.png`} />
          {weather} °C</p>
        <p>{description}</p>
        <p>{curTime}</p>
      </div>
    );
  }
}


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Today's Weather is...</h1>
        </header>
        <WeatherApp />
      </div>
    );
  }
}

export default App;